import { Component, Input, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { timer } from 'rxjs';
import { IPump } from '../interfaces/pump.interface';
import { PumpService } from '../services/pump-service.service';

@Component({
  selector: 'app-pumps-page',
  templateUrl: './pumps-page.component.html',
  styleUrls: ['./pumps-page.component.scss']
})
export class PumpsPageComponent implements OnInit {

  constructor(private route: ActivatedRoute, private pumpService: PumpService) { }
  pumpState: boolean = false
  connectState: boolean = false
  pumpNumber: number
  timerOn: boolean = false
  subscription
  indicators = []
  startDate: Date
  endDate: Date
  ngOnInit(): void {
    this.pumpNumber = this.route.snapshot.params.id
  }

  createAllData() {
    if (this.connectState == false) {
      this.pumpService.createDate(this.pumpNumber).subscribe(
        response => {
          console.log(response)
        },
        error => {
          console.log(error)
        }
      )
    } else {
      this.pumpService.closeConnect(this.pumpNumber).subscribe(
        response => {
          console.log(response)
        },
        error => {
          console.log(error)
        }
      )
    }

  }

  getData() {
    this.pumpService.getIndicators(this.pumpNumber).subscribe(
      response => {
        this.indicators = response
      },
      error => {
        console.log(error)
      }
    )
  }

  pumpOn() {
    this.pumpState = !this.pumpState
    this.time(true)
  }

  time(state: boolean) {
    const numbers = timer(0, 5000);
    if (state === true) {

      this.subscription = numbers.subscribe(
        x => {
          this.createAllData()
          this.getData()
        });
    } else {
      this.subscription.unsubscribe()
    }
  }

  pumpOff() {
    this.pumpState = !this.pumpState
    this.time(false)
  }

  getDateIndicators() {
    console.log(this.startDate.getTime())
    this.pumpService.getIndicatorsWithDate(this.pumpNumber, this.startDate.toJSON(), this.endDate.toJSON())

      .subscribe(
        response => {
          this.indicators = response
        },
        error => {
          console.log(error)
        }
      )
  }

  connectionOn() {
    this.connectState = true
    this.time(true)

  }
  connectionOff() {
    this.connectState = false
    this.time(false)
    this.pumpService.openConnect(this.pumpNumber).subscribe(
      response => {
        console.log(response)
      }, error => {
        console.log(error)
      }
    )
  }
}
