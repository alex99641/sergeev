import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-pumps',
  templateUrl: './pumps.component.html',
  styleUrls: ['./pumps.component.scss']
})
export class PumpsComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

  @Input() pump
}
