import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { MainPageComponent } from 'src/app/main-page/main-page.component'
import { PumpsPageComponent } from 'src/app/pumps-page/pumps-page.component'
const routes: Routes = [
  { path: '', component: MainPageComponent },
  { path: 'pump/:id', component: PumpsPageComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
