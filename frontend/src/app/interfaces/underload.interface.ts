export interface IPressure {
    id: number
    wellhead_pressure: boolean
    time: Date
}