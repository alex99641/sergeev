export interface IOverload {
    id: number
    overload: boolean
    time: Date
}