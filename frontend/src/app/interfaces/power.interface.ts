export interface IPower {
    id: number
    active_power: number
    time: Date
}