export interface IForce {
    id: number
    force_rod: number
    time: Date
}