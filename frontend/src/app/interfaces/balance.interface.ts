export interface IBalance {
    id: number
    balance_position: number
    time: Date
}