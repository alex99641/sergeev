export interface IUnderload {
    id: number
    underload: boolean
    time: Date
}