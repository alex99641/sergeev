export interface IPump {
    id: number
    pumpnumber: number
    pumpState: boolean
}