export interface ILiquid {
    id: number
    liquid_level: number
    time: Date
}