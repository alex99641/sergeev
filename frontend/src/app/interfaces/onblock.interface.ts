export interface IOnBlock {
    id: number
    onblock: boolean
    time: Date
}