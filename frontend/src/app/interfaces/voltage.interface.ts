export interface IVoltage {
    id: number
    no_voltage: boolean
    time: Date
}