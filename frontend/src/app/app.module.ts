import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule } from '@angular/common/http';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { MainPageComponent } from './main-page/main-page.component';
import { PumpsComponent } from './pumps/pumps.component';
import { PumpsPageComponent } from './pumps-page/pumps-page.component';
import { MatButtonModule } from '@angular/material/button';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { GoogleChartsModule } from 'angular-google-charts';
import { ForceChartComponent } from './charts/force-chart/force-chart.component';
import { LiquidChartComponent } from './charts/liquid-chart/liquid-chart.component';
import { BalanceChartComponent } from './charts/balance-chart/balance-chart.component';
import { VoltageChartComponent } from './charts/voltage-chart/voltage-chart.component';
import { PressureChartComponent } from './charts/pressure-chart/pressure-chart.component';
import { OverloadChartComponent } from './charts/overload-chart/overload-chart.component';
import { UnderloadChartComponent } from './charts/underload-chart/underload-chart.component';
import { OnblockChartComponent } from './charts/onblock-chart/onblock-chart.component';
import { PowerChartComponent } from './charts/power-chart/power-chart.component';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatNativeDateModule } from '@angular/material/core';
import { FormsModule } from '@angular/forms';

@NgModule({
  declarations: [
    AppComponent,
    MainPageComponent,
    PumpsComponent,
    PumpsPageComponent,
    ForceChartComponent,
    LiquidChartComponent,
    BalanceChartComponent,
    VoltageChartComponent,
    PressureChartComponent,
    OverloadChartComponent,
    UnderloadChartComponent,
    OnblockChartComponent,
    PowerChartComponent
  ],
  imports: [
    GoogleChartsModule,
    BrowserModule,
    MatButtonModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    HttpClientModule,
    MatDatepickerModule,
    MatNativeDateModule,
    FormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
