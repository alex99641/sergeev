import { TestBed } from '@angular/core/testing';

import { PumpServiceService } from './pump-service.service';

describe('PumpServiceService', () => {
  let service: PumpServiceService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(PumpServiceService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
