import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment'
@Injectable({
  providedIn: 'root'
})
export class PumpService {
  url: string = environment.url
  constructor(private http: HttpClient) { }

  createDate(idPump: number): Observable<any> {
    const params = new HttpParams().set('id', idPump.toString());
    return this.http.get(this.url + '/create/data', { params })
  }

  getPumps(): Observable<any> {
    return this.http.get(this.url + '/get/pumps')
  }

  getIndicators(idPump): Observable<any> {
    const params = new HttpParams().set('id', idPump.toString());
    return this.http.get(this.url + '/get/indicators', { params })
  }

  getIndicatorsWithDate(id: number, start: string, end: string): Observable<any> {
    let object = {
      id: id,
      start: start.toString(),
      endDate: end.toString()
    }
    return this.http.post(this.url + '/post/indicators/date', object)
  }

  closeConnect(idPump): Observable<any> {
    const params = new HttpParams().set('id', idPump.toString());
    return this.http.get(this.url + '/connection/down', { params })
  }

  openConnect(idPump): Observable<any> {
    const params = new HttpParams().set('id', idPump.toString());
    return this.http.get(this.url + '/connection/up', { params })
  }
}
