import { Component, OnInit } from '@angular/core';
import { PumpService } from 'src/app/services/pump-service.service'
import { IPump } from '../interfaces/pump.interface';
@Component({
  selector: 'app-main-page',
  templateUrl: './main-page.component.html',
  styleUrls: ['./main-page.component.scss']
})
export class MainPageComponent implements OnInit {
  pumps: IPump[] = []
  constructor(private pumpService: PumpService) { }

  ngOnInit(): void {
    this.pumpService.getPumps().subscribe(
      response => {
        this.pumps = response
        console.log(response)
      },
      error => {
        console.log(error)
      }
    )

  }

}
