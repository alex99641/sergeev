import { Component, Input, OnInit } from '@angular/core';
import { IVoltage } from 'src/app/interfaces/voltage.interface';

@Component({
  selector: 'app-voltage-chart',
  templateUrl: './voltage-chart.component.html',
  styleUrls: ['./voltage-chart.component.css']
})
export class VoltageChartComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }
  title = 'Отсутствие напряжения на вводе СУ';
  type = 'ColumnChart';
  data = [
    ["0", 0],
  ];

  options = {
    vAxis: {
      viewWindowMode: 'explicit',
      viewWindow: {
        max: 1,
        min: -1
      }
    }
  };
  width = 550;
  height = 400;
  newData: IVoltage[] = []
  ngOnChanges() {
    this.newData = this.indicators
    if (this.newData.length > 1) {
      this.data = []
      for (let i = 0; i < this.newData.length; i++) {
        this.data.push(['', this.boolToInt(this.newData[i].no_voltage)])
      }
    }
  }
  boolToInt(bool) {
    if (bool) {
      return 1;
    } else {
      return -1
    }
  }
  @Input() indicators
}
