import { Component, Input, OnInit } from '@angular/core';
import { IPower } from 'src/app/interfaces/power.interface';

@Component({
  selector: 'app-power-chart',
  templateUrl: './power-chart.component.html',
  styleUrls: ['./power-chart.component.css']
})
export class PowerChartComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }
  title = 'Активная мощность';
  type = 'AreaChart';
  data = [
    ["0", 0],
  ];

  options = {
    vAxis: {
      viewWindowMode: 'explicit',
      viewWindow: {
        max: 100,
        min: 0
      }
    }
  };
  width = 550;
  height = 400;
  newData: IPower[] = []
  ngOnChanges() {
    this.newData = this.indicators
    if (this.newData.length > 1) {
      this.data = []
      for (let i = 0; i < this.newData.length; i++) {
        this.data.push(['', Number(this.newData[i].active_power)])
      }
    }
  }
  @Input() indicators
}
