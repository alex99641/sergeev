import { Component, Input, OnInit } from '@angular/core';
import { IBalance } from 'src/app/interfaces/balance.interface';

@Component({
  selector: 'app-balance-chart',
  templateUrl: './balance-chart.component.html',
  styleUrls: ['./balance-chart.component.css']
})
export class BalanceChartComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }
  title = 'Положение балансира';
  type = 'AreaChart';
  data = [
    ["0", 0],
  ];

  options = {
    vAxis: {
      viewWindowMode: 'explicit',
      viewWindow: {
        max: 100,
        min: 0
      }
    }
  };
  width = 550;
  height = 400;

  newData: IBalance[] = []
  ngOnChanges() {
    this.newData = this.indicators
    if (this.newData.length > 1) {
      this.data = []
      for (let i = 0; i < this.newData.length; i++) {
        this.data.push(['', this.newData[i].balance_position])
      }
    }
  }
  @Input() indicators: any
}
