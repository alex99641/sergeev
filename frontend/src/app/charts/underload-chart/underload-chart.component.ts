import { Component, Input, OnInit } from '@angular/core';
import { IUnderload } from 'src/app/interfaces/pressure.interface';

@Component({
  selector: 'app-underload-chart',
  templateUrl: './underload-chart.component.html',
  styleUrls: ['./underload-chart.component.css']
})
export class UnderloadChartComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }
  title = 'Недогруз';
  type = 'ColumnChart';
  data = [
    ["0", 0],
  ];

  options = {
    vAxis: {
      viewWindowMode: 'explicit',
      viewWindow: {
        max: 1,
        min: -1
      }
    }
  };
  width = 550;
  height = 400;
  newData: IUnderload[] = []
  ngOnChanges() {
    this.newData = this.indicators
    if (this.newData.length > 1) {
      this.data = [['0', 0]]
      for (let i = 0; i < this.newData.length; i++) {
        this.data.push(['', this.boolToInt(this.newData[i].underload)])
      }
    }
  }
  boolToInt(bool) {
    if (bool) {
      return 1;
    } else {
      return -1
    }
  }
  @Input() indicators
}
