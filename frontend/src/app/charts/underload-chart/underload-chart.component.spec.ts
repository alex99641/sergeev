import { ComponentFixture, TestBed } from '@angular/core/testing';

import { UnderloadChartComponent } from './underload-chart.component';

describe('UnderloadChartComponent', () => {
  let component: UnderloadChartComponent;
  let fixture: ComponentFixture<UnderloadChartComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ UnderloadChartComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(UnderloadChartComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
