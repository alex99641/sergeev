import { Component, Input, OnInit } from '@angular/core';
import { IPressure } from 'src/app/interfaces/underload.interface';

@Component({
  selector: 'app-pressure-chart',
  templateUrl: './pressure-chart.component.html',
  styleUrls: ['./pressure-chart.component.css']
})
export class PressureChartComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }
  title = 'Давление устьевое';
  type = 'ColumnChart';
  data = [
    ["0", 0],
  ];

  options = {
    vAxis: {
      viewWindowMode: 'explicit',
      viewWindow: {
        max: 1,
        min: -1
      }
    }
  };
  width = 550;
  height = 400;
  newData: IPressure[] = []
  ngOnChanges() {
    this.newData = this.indicators
    if (this.newData.length > 1) {
      this.data = []
      for (let i = 0; i < this.newData.length; i++) {
        this.data.push(['', this.boolToInt(this.newData[i].wellhead_pressure)])
      }
    }
  }
  boolToInt(bool) {
    if (bool) {
      return 1;
    } else {
      return -1
    }
  }
  @Input() indicators
}
