import { ComponentFixture, TestBed } from '@angular/core/testing';

import { OverloadChartComponent } from './overload-chart.component';

describe('OverloadChartComponent', () => {
  let component: OverloadChartComponent;
  let fixture: ComponentFixture<OverloadChartComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ OverloadChartComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(OverloadChartComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
