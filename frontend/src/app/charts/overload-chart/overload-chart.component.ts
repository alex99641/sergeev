import { Component, Input, OnInit } from '@angular/core';
import { IOverload } from 'src/app/interfaces/overload.interface';

@Component({
  selector: 'app-overload-chart',
  templateUrl: './overload-chart.component.html',
  styleUrls: ['./overload-chart.component.css']
})
export class OverloadChartComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }
  title = 'Перегруз';
  type = 'ColumnChart';
  data = [
    ["0", 0],
  ];

  options = {
    vAxis: {
      viewWindowMode: 'explicit',
      viewWindow: {
        max: 1,
        min: -1
      }
    }
  };
  width = 550;
  height = 400;
  newData: IOverload[] = []
  ngOnChanges() {
    this.newData = this.indicators
    if (this.newData.length > 1) {
      this.data = []
      for (let i = 0; i < this.newData.length; i++) {
        this.data.push(['', this.boolToInt(this.newData[i].overload)])
      }
    }
  }
  boolToInt(bool) {
    if (bool) {
      return 1;
    } else {
      return -1
    }
  }
  @Input() indicators
}
