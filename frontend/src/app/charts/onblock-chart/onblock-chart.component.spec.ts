import { ComponentFixture, TestBed } from '@angular/core/testing';

import { OnblockChartComponent } from './onblock-chart.component';

describe('OnblockChartComponent', () => {
  let component: OnblockChartComponent;
  let fixture: ComponentFixture<OnblockChartComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ OnblockChartComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(OnblockChartComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
