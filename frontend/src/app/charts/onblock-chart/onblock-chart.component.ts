import { Component, Input, OnInit } from '@angular/core';
import { IOnBlock } from 'src/app/interfaces/onblock.interface';

@Component({
  selector: 'app-onblock-chart',
  templateUrl: './onblock-chart.component.html',
  styleUrls: ['./onblock-chart.component.css']
})
export class OnblockChartComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }
  title = 'Деблокировка';
  type = 'ColumnChart';
  data = [
    ["0", 0],
  ];

  options = {
    vAxis: {
      viewWindowMode: 'explicit',
      viewWindow: {
        max: 1,
        min: -1
      }
    }
  };
  width = 550;
  height = 400;
  newData: IOnBlock[] = []
  ngOnChanges() {
    this.newData = this.indicators
    if (this.newData.length > 1) {
      this.data = []
      for (let i = 0; i < this.newData.length; i++) {
        this.data.push(['', this.boolToInt(this.newData[i].onblock)])
      }
    }
  }
  boolToInt(bool) {
    if (bool) {
      return 1;
    } else {
      return -1
    }
  }
  @Input() indicators
}
