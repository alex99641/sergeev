import { Component, Input, OnInit } from '@angular/core';
import { ILiquid } from 'src/app/interfaces/liquid.interface';

@Component({
  selector: 'app-liquid-chart',
  templateUrl: './liquid-chart.component.html',
  styleUrls: ['./liquid-chart.component.css']
})
export class LiquidChartComponent implements OnInit {
  data = [
    ['0', 0],
  ];

  constructor() { }

  ngOnInit(): void {
  }

  title = 'Динамический уровень жидкости';
  type = 'AreaChart';

  options = {
    vAxis: {
      viewWindowMode: 'explicit',
      viewWindow: {
        max: 100,
        min: 0
      }
    }
  };
  width = 550;
  height = 400;
  newData: ILiquid[] = []
  ngOnChanges() {
    this.newData = this.indicators
    if (this.newData.length > 1) {
      this.data = []
      for (let i = 0; i < this.newData.length; i++) {
        this.data.push(['', this.newData[i].liquid_level])
      }
    }
  }
  @Input() indicators: ILiquid[]
}
