import { ComponentFixture, TestBed } from '@angular/core/testing';

import { LiquidChartComponent } from './liquid-chart.component';

describe('LiquidChartComponent', () => {
  let component: LiquidChartComponent;
  let fixture: ComponentFixture<LiquidChartComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ LiquidChartComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(LiquidChartComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
