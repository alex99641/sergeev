import { Component, Input, OnInit } from '@angular/core';
import { IForce } from 'src/app/interfaces/force.interface';

@Component({
  selector: 'app-force-chart',
  templateUrl: './force-chart.component.html',
  styleUrls: ['./force-chart.component.css']
})
export class ForceChartComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }
  title = 'Усилие в полированном штоке';
  type = 'AreaChart';
  data = [
    ["0", 0],
  ];

  options = {
    vAxis: {
      viewWindowMode: 'explicit',
      viewWindow: {
        max: 100,
        min: 0
      }
    }
  };
  width = 550;
  height = 400;
  newData: IForce[] = []
  ngOnChanges() {
    this.newData = this.indicators
    if (this.newData.length > 1) {
      this.data = []
      for (let i = 0; i < this.newData.length; i++) {
        this.data.push(['', this.newData[i].force_rod])
      }
    }
  }
  @Input() indicators

}
