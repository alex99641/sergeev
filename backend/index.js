const express = require('express')
const mysql = require("mysql2");
const cors = require('cors');
const app = express()
const PORT = 4000
const liquid = require('./modules/liquid_module')
const force = require('./modules/force_module')
const onblock = require('./modules/onblock_module')
const balance = require('./modules/balance_module')
const overload = require('./modules/overload_module')
const pumpState = require('./modules/pump_module')
const activePower = require('./modules/active_power_module')
const underload = require('./modules/underload_module')
const voltage = require('./modules/voltage_module')
const wellhead = require('./modules/wellhead_module')
const jsonParser = express.json();
let buffer = []
let counter = 0;
app.use(cors())

const connection = mysql.createConnection({
    host: "localhost",
    user: "root",
    database: "mydb",
    password: "password",
    port: 6612
});

app.use(express.static(__dirname + "/public"));


// http:/localhost:4000/create/data
app.get("/create/data", function (request, response) {
    const insertSql = `INSERT INTO indicators (
        liquid_level, 
        force_rod, 
        balance_position, 
        no_voltage,
        wellhead_pressure, 
        overload,
        underload,
        onblock,
        active_power, 
        time, 
        pump_id) 
        VALUES(?,?,?,?,?,?,?,?,?,?,?)`;
    let time = new Date()
    let idPump = parseInt(request.query.id)
    let indicators = createData(idPump, time)

    connection.connect(function (err) {
        connection.query(insertSql, indicators, function (err) {
            if (err) return console.log(err);
        });
    })
    response.send(indicators)
})

app.get("/get/pumps", function (request, response) {
    const Sql = `SELECT * FROM pumps`;
    connection.connect(function (err) {
        connection.query(Sql, function (err, data) {
            if (err) return console.log(err);
            response.json(data)
        });
    })
})

app.get("/get/indicators", function (request, response) {
    let idPump = parseInt(request.query.id)
    const Sql = `SELECT * FROM indicators WHERE pump_id=${idPump}`;
    connection.connect(function (err) {
        connection.query(Sql, function (err, data) {
            if (err) return console.log(err);
            response.json(data)
        });
    })
})

app.post("/post/indicators/date", jsonParser, function (request, response) {
    let start = new Date(request.body.start).getTime()
    let end = new Date(request.body.endDate).getTime()
    let idPump = parseInt(request.body.id)
    const Sql = `SELECT * FROM indicators WHERE pump_id=${idPump}`;
    connection.connect(function (err) {
        connection.query(Sql, function (err, data) {
            if (err) return console.log(err);
            let filteredData = data.filter(function (arr, index) {
                if (arr.time >= start && arr.time <= end)
                    return arr
            })
            response.json(filteredData)
        });
    })
})

app.get("/connection/down", function (request, response) {
    let idPump = parseInt(request.query.id)
    counter++;
    let time = new Date()
    buffer.push(createData(idPump, time))
    if (buffer.length == 10) {
        deleteBuffer()
    }
})

function deleteBuffer() {
    for (let int = 0; i < buffer.length; i++) {
        if (i % 2 != 0) {
            buffer.splice(i, 1)
        }
    }
}

app.get("/connection/up", function (request, response) {
    const insertSql = `INSERT INTO indicators (
        liquid_level, 
        force_rod, 
        balance_position, 
        no_voltage,
        wellhead_pressure, 
        overload,
        underload,
        onblock,
        active_power, 
        time, 
        pump_id) 
        VALUES(?,?,?,?,?,?,?,?,?,?,?)`;

    for (let i = 0; i < buffer.length; i++) {
        connection.connect(function (err) {
            connection.query(insertSql, buffer[i], function (err) {
                if (err) return console.log(err);
            });
        })
    }

})

function createData(id, date) {
    data = [
        liquid.GetData(),
        force.GetData(),
        balance.GetData(),
        voltage.GetData(),
        wellhead.GetData(),
        overload.GetData(),
        underload.GetData(),
        onblock.GetData(),
        Math.ceil(activePower.GetData()),
        date,
        id
    ]
    return data;
}

app.post("/data", function (request, response) {

})

app.listen(PORT)