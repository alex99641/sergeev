function normal(min, max, skew) {
    let u1 = 0, u2 = 0;
    while (u1 === 0) u1 = Math.random();
    while (u2 === 0) u2 = Math.random();
    let num = Math.sqrt(-2.0 * Math.log(u1)) * Math.cos(2.0 * Math.PI * u2)

    num = num / 10.0 + 0.5;
    if (num > 1 || num < 0) num = randn_bm(min, max, skew);
    num *= max - min;
    num += min;
    return num;
}

function getRandomIntInclusive(min, max) {
    min = Math.ceil(min);
    max = Math.floor(max);
    let result = Math.floor(Math.random() * (max - min + 1)) + min;
    if (result == 0) return normal(61, 100, 1)
    else return normal(30, 60, 1)
}

function GetData() {
    return Math.ceil(getRandomIntInclusive(0, 10))
}

module.exports = {
    GetData: GetData
}