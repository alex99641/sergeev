function getRandomIntInclusive(min, max) {
    min = Math.ceil(min);
    max = Math.floor(max);
    return Math.floor(Math.random() * (max - min + 1)) + min;
}

function GetData() {
    let result = getRandomIntInclusive(0, 10)
    if (result == 0) return true
    else return false
}

module.exports = {
    GetData: GetData
}